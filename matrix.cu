#include <iostream>
#include <stdlib.h>
#include <sys/time.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <math.h>

#define ROWS 1024
#define COLS 1024

using namespace std;


void matrix_mul_cpu(float* M, float* N, float* P, int width){

    for(int i = 0; i < width; ++i){
        for (int j = 0; j < width; ++j){
            float sum = 0.;
            for (int k =0; k< width; k++){
                float a = M[i * width + k];
                float b = N[k * width + j];
                sum += a * b;
            }
            P[i*width + j] = sum;
        }
    }
}

__global__ void matrix_mul_gpu(float *M, float *N, float *P, int width){
    int i = threadIdx.x + blockDim.x * blockIdx.x;
    int j = threadIdx.y + blockDim.y * blockIdx.y;

    int sum = 0;
    for (int k=0; k < width; k++){
        float a = M[j * width + k];
        float b = N[k * width + i];
        sum += a * b;
    }
    P[j * width + i] = sum;
}





int main() {

    struct timeval start,end;
    gettimeofday(&start, NULL);

    float *A,*B,*C;
    int total_size = ROWS*COLS*sizeof(float);
    A = (float*)malloc(total_size);
    B = (float*)malloc(total_size);
    C = (float*)malloc(total_size);

    float *d_A,*d_B,*d_C;
    cudaMalloc((void**)&d_A,sizeof(float) * ROWS * COLS);
    cudaMalloc((void**)&d_B,sizeof(float) * ROWS * COLS);
    cudaMalloc((void**)&d_C,sizeof(float) * ROWS * COLS);

    for(int i=0; i< ROWS * COLS;++i){
        A[i] = 80.0;
        B[i] = 20.0;

    }

    cudaMemcpy(d_A,A,sizeof(float) * ROWS * COLS, cudaMemcpyHostToDevice);
    cudaMemcpy(d_B,B,sizeof(float) * ROWS * COLS, cudaMemcpyHostToDevice);

    dim3 threadPerBlock(16,16);
    dim3 blockNumber((COLS + threadPerBlock.x - 1) / threadPerBlock.x, (ROWS + threadPerBlock.y - 1) / threadPerBlock.y);
    printf("Block(%d,%d)  Grid(%d,%d).\n",threadPerBlock.x,threadPerBlock.y,blockNumber.x,blockNumber.y);

    matrix_mul_gpu<<<blockNumber, threadPerBlock>>> (d_A,d_B,d_C,COLS);

    cudaMemcpy(C,d_C,sizeof(float) * ROWS * COLS, cudaMemcpyDeviceToHost);

    free(A);
    free(B);
    free(C);
    
    cudaFree(d_A);
    cudaFree(d_B);
    cudaFree(d_C);


    // matrix_mul_cpu(A,B,C,COLS);

    gettimeofday(&end,NULL);

    int timeuse = 1000000 * (end.tv_sec - start.tv_sec) + end.tv_usec - start.tv_usec;
    cout << "total time is " << timeuse / 1000 << "ms" << endl;
    return 0;

}
